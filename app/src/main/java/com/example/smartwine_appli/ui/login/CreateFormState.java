package com.example.smartwine_appli.ui.login;

import android.support.annotation.Nullable;

/**
 * Data validation state of the login form.
 */
class CreateFormState {
    @Nullable
    private Integer usernameError;
    @Nullable
    private Integer passwordError;
    @Nullable
    private Integer confirmpasswordError;
    private boolean isDataValid;

    CreateFormState(@Nullable Integer usernameError, @Nullable Integer passwordError, @Nullable Integer confirmpasswordError) {
        this.usernameError = usernameError;
        this.passwordError = passwordError;
        this.confirmpasswordError = confirmpasswordError;
        this.isDataValid = false;
    }

    CreateFormState(boolean isDataValid) {
        this.usernameError = null;
        this.passwordError = null;
        this.confirmpasswordError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getUsernameError() {
        return usernameError;
    }

    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }

    @Nullable
    Integer getConfirmpasswordError(){ return confirmpasswordError;}

    boolean isDataValid() {
        return isDataValid;
    }
}
