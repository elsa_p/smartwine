package com.example.smartwine_appli.ui.login;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartwine_appli.MainActivity;
import com.example.smartwine_appli.R;
import com.example.smartwine_appli.data.DatabaseHandler;
import com.example.smartwine_appli.user_wine.User;
import com.example.smartwine_appli.data.DatabaseUser;
import com.example.smartwine_appli.user_wine.Wine;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    DatabaseUser myDbUser;
    DatabaseHandler myDbWine;

    public User user1;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        myDbUser = new DatabaseUser(this);
        myDbWine = new DatabaseHandler(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final Button createaccountButton = findViewById(R.id.create_account);
        final EditText createusername = findViewById(R.id.create_username);
        final EditText createpassword = findViewById(R.id.create_password);
        final EditText confirmpassword = findViewById(R.id.confirm_password);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = usernameEditText.getText().toString();
                String userPassword = passwordEditText.getText().toString();
                authentification(userName, userPassword);



            }
        });

        loginViewModel.getCreateFormState().observe(this, new Observer<CreateFormState>() {
            @Override
            public void onChanged(@Nullable CreateFormState createFormState) {
                if (createFormState == null) {
                    return;
                }
                if(createFormState.isDataValid()){
                    if(createpassword.getText().toString().equals(confirmpassword.getText().toString())){
                        createaccountButton.setEnabled(true);
                    }
                }else{
                    createaccountButton.setEnabled(false);
                }
                //createaccountButton.setEnabled(createFormState.isDataValid());
                if (createFormState.getUsernameError() != null) {
                    createusername.setError(getString(createFormState.getUsernameError()));
                }
                if (createFormState.getPasswordError() != null) {
                    createpassword.setError(getString(createFormState.getPasswordError()));
                }
                if (createFormState.getConfirmpasswordError() != null) {
                    confirmpassword.setError(getString(createFormState.getConfirmpasswordError()));
                }
            }
        });

        TextWatcher afterTextChangedListener_creationaccount = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.createDataChanged(createusername.getText().toString(),
                        createpassword.getText().toString(), confirmpassword.getText().toString());
            }
        };
        createusername.addTextChangedListener(afterTextChangedListener_creationaccount);
        createpassword.addTextChangedListener(afterTextChangedListener_creationaccount);
        confirmpassword.addTextChangedListener(afterTextChangedListener_creationaccount);

        /*if((createpassword.getText().toString()).equals(confirmpassword.getText().toString()) & !(createusername.getText().toString()).equals("")){
            createaccountButton.setEnabled(true);
        }else{
            createaccountButton.setEnabled(false);
        }*/

        createaccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userPassword = createpassword.getText().toString();
                String confirmedPassword = confirmpassword.getText().toString();
                String userName = createusername.getText().toString();
                createAccount(userName, userPassword, confirmedPassword);
            }
        });
    }

    private void createAccount(String userName, String userPassword, String confirmedPassword){
        if(userPassword.equals(confirmedPassword)){
            if(myDbUser.insertUser(userName, userPassword)) {

                Toast.makeText(LoginActivity.this, "User created", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(LoginActivity.this, "User not created", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(LoginActivity.this, "User not created", Toast.LENGTH_LONG).show();
        }
    }
    private void authentification(String userName, String userPasseword){
        /*Cursor res_user = myDbUser.getUserByIDandPassword(userName, userPasseword);
        if(res_user.getCount() > 0){
            Toast.makeText(LoginActivity.this, "User detected", Toast.LENGTH_LONG).show();
            user1 = new User(userName, userPasseword);

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("id_user", user1.getId());
            startActivity(intent);

        }else{
            Toast.makeText(LoginActivity.this, "User not detected", Toast.LENGTH_LONG).show();
        }*/
        Cursor res_user = myDbUser.getUserByIDandPassword(userName, userPasseword);
        ArrayList<Wine> wineList= new ArrayList<>();
        Wine wine = new Wine();

        if(res_user.getCount() > 0){
            Toast.makeText(LoginActivity.this, "User detected", Toast.LENGTH_LONG).show();
            user1 = new User(userName, userPasseword);

            //récup les vins associés à ce user
            Cursor res = myDbWine.getWineByUser(userName);
            if(res.getCount()!=0) {
                while (res.moveToNext()) {
                    wine.setPosition(res.getString(5));
                    wine.setMeal(res.getString(4));
                    wine.setColor(res.getString(3));
                    wine.setYear(res.getString(2));
                    wine.setName(res.getString(1));
                    wine.setId(res.getString(0));

                    wineList.add(wine);
                    /*wineList.get(i).setPosition(position);
                    wineList.get(i).setColor(color);
                    wineList.get(i).setName(name);
                    wineList.get(i).setId(id_wine);
                    wineList.get(i).setYear(year);
                    wineList.get(i).setMeal(meal);
                    wineList.get(i).setIdUser(userName);*/
                    user1.setId(userName);
                    user1.setPassword(userPasseword);
                    user1.setWines(wineList);
                    Log.e("******* ", "Winelist du user: "+wineList.size());
                }

            }

            //fin des changements

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("id_user", user1.getId());
            startActivity(intent);

        }else{
            Toast.makeText(LoginActivity.this, "User not detected", Toast.LENGTH_LONG).show();
        }
    }
    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
