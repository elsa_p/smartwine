package com.example.smartwine_appli;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Locale;

public class Filter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);


        /******************
         //rajouter checkbox des repas
         ****************/



        //send data filter to list activity
        final Button apply_filter = findViewById(R.id.apply_filter);
        apply_filter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                Intent intent = new Intent(Filter.this, ListActivity.class);

                EditText year = findViewById(R.id.edit_year);
                final String string_year = year.getText().toString().toLowerCase(Locale.getDefault());
                if(string_year!=""){
                    Log.e("**** : "+ string_year, " year : " + string_year);

                    intent.putExtra("string_year", string_year);
                }


                final CheckBox cb_rouge = (CheckBox) findViewById(R.id.cb_rouge);
                final String string_red;
                final String string_white;
                final String string_pink;
                final String string_aperitif;
                final String string_meat;
                final String string_fish ;
                final String string_cheese;
                final String string_dessert;

                if (cb_rouge.isChecked()) {
                    string_red = cb_rouge.getText().toString();
                    intent.putExtra("string_red", string_red);

                    Log.e("**** : "+ string_red, " color : " + string_red);
                }else{
                    string_red = "";
                }
                final CheckBox cb_blanc = (CheckBox) findViewById(R.id.cb_blanc);
                if (cb_blanc.isChecked()) {
                    string_white = cb_blanc.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_white", string_white);

                }else{
                    string_white = "";
                }
                final CheckBox cb_rose = (CheckBox) findViewById(R.id.cb_rose);
                if (cb_rose.isChecked()) {
                    string_pink = cb_rose.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_pink", string_pink);

                }else{
                    string_pink = "";
                }

                //repas
                final CheckBox cb_aperitif = (CheckBox) findViewById(R.id.cb_apero);
                if (cb_aperitif.isChecked()) {
                    string_aperitif = cb_aperitif.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_aperitif", string_aperitif);

                }else{
                    string_aperitif = "";
                }
                final CheckBox cb_meat = (CheckBox) findViewById(R.id.cb_viande);
                if (cb_meat.isChecked()) {
                    string_meat= cb_meat.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_meat", string_meat);

                }else{
                    string_meat = "";
                }
                final CheckBox cb_fish = (CheckBox) findViewById(R.id.cb_poisson);
                if (cb_fish.isChecked()) {
                    string_fish = cb_fish.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_fish", string_fish);

                }else{
                    string_fish = "";
                }
                final CheckBox cb_cheese = (CheckBox) findViewById(R.id.cb_fromage);
                if (cb_cheese.isChecked()) {
                    string_cheese = cb_cheese.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_cheese", string_cheese);

                }else{
                    string_cheese = "";
                }
                final CheckBox cb_dessert = (CheckBox) findViewById(R.id.cb_dessert);
                if (cb_dessert.isChecked()) {
                    string_dessert = cb_dessert.getText().toString().toLowerCase(Locale.getDefault());
                    intent.putExtra("string_dessert", string_dessert);

                }else{
                    string_dessert = "";
                }

                Log.e("**** : "+ string_red, " color : " + string_red);


                startActivity(intent);
            }
        });


    }

//    public void onCheckboxClicked(View view) {
//        // Is the view now checked?
//        boolean checked = ((CheckBox) view).isChecked();
//        final CheckBox cb_rouge = (CheckBox) findViewById(R.id.cb_rouge);
//        final String string_red;
//        // Check which checkbox was clicked
//        switch(view.getId()) {
//            case R.id.cb_rouge:
//                if (checked){
//                    string_red = cb_rouge.getText().toString();
//
//                    Log.e("**** : "+ string_red, " color : " + string_red);
//                }
//                else
//                    string_red = "";
//                break;
//            case R.id.cb_blanc:
//                if (checked){
//                    colorbottle.setImageResource(R.mipmap.white);
//                }
//                else
//                    colorbottle.setImageResource(R.mipmap.dark);
//                break;
//            case R.id.cb_rose:
//                if (checked){
//                    colorbottle.setImageResource(R.mipmap.pink);
//
//                }
//                else
//                    colorbottle.setImageResource(R.mipmap.dark);
//                break;
//            // TODO: Veggie sandwich
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

        */
        Intent intent = new Intent(Filter.this, ListActivity.class);
        startActivity(intent);
        return true;
    }
}
