package com.example.smartwine_appli;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.smartwine_appli.data.DatabaseHandler;
import com.example.smartwine_appli.data.DatabaseUser;
import com.example.smartwine_appli.ui.login.LoginActivity;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    DatabaseHandler myDb;
    DatabaseUser myDbUser;
    private Button x1y1;
    private Button x1y2;
    private Button x1y3;
    private Button x2y1;
    private Button x2y2;
    private Button x2y3;
    ArrayList<Button> list = new ArrayList<Button>();





    private Toolbar mToolbar;
    static public String id_user;
    static public String str_id_bottle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDb = new DatabaseHandler(this);
        myDbUser = new DatabaseUser(this);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("id_user")) {
                id_user = intent.getStringExtra("id_user");
            }
        }
       // Cursor res_user = myDbUser.getAllUser();
       // Log.e("*******", "user inserted : "+res_user.getCount());
        //myDb.deleteData("8715342007267");
        //myDb.deleteData("070330512122");
        setContentView(R.layout.test);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        myDb.deleteAllData();
        Cursor res = myDb.getAllData();
        Log.e("*****", "Nombre de vin : "+res.getCount());


        list.add((Button) findViewById(R.id.x1y1));
        list.add((Button) findViewById(R.id.x1y2));
        list.add((Button) findViewById(R.id.x1y3));
        list.add((Button) findViewById(R.id.x2y1));
        list.add((Button) findViewById(R.id.x2y2));
        list.add((Button) findViewById(R.id.x2y3));
        list.add((Button) findViewById(R.id.x3y1));
        list.add((Button) findViewById(R.id.x3y2));
        list.add((Button) findViewById(R.id.x3y3));
        list.add((Button) findViewById(R.id.x4y1));
        list.add((Button) findViewById(R.id.x4y2));
        list.add((Button) findViewById(R.id.x4y3));


        final FloatingActionButton fab_list = findViewById(R.id.fab_list);
        fab_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putExtra("id_user", id_user);
                startActivity(intent);
            }
        });

        final FloatingActionButton fab_scan = findViewById(R.id.fab_scan);
        fab_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ReadingBarCode.class);
                startActivity(intent);
            }
        });

        if (intent.hasExtra("edittext")) { // vérifie qu'une valeur est associée à la clé “edittext”
            str_id_bottle = intent.getStringExtra("edittext"); // on récupère la valeur associée à la clé
            ChooseWineByScan(str_id_bottle);
        }



        x1y1 = findViewById(R.id.x1y1);
        x1y2 = findViewById(R.id.x1y2);
        x1y3 = findViewById(R.id.x1y3);
        x2y1 = findViewById(R.id.x2y1);
        x2y2 = findViewById(R.id.x2y2);
        x2y3 = findViewById(R.id.x2y3);
        UpdateCave();
//        x1y1.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // Code here executes on main thread after user presses button
//                if(x1y1.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.rounded_button_grey).getConstantState()))
//                {
//                    Intent intent = new Intent(MainActivity.this, AddWine.class);
//                    intent.putExtra("position", "x1y1");
//                    startActivity(intent);
//                }else{
//                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
//                    intent.putExtra("position", "x1y1");
//                    startActivity(intent);
//                }
//
//            }
//        });
//        x1y2.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // Code here executes on main thread after user presses button
//                //Log.e("**** : "+but12.getBackground().getConstantState(),"  => " + ContextCompat.getDrawable(getApplicationContext(),R.drawable.rounded_button_grey).getConstantState());
//               // Log.e("**** :"+but12.getBackground(),"  => " + ContextCompat.getDrawable(getApplicationContext(),R.drawable.rounded_button_grey)) ;
//
//                if(x1y2.getBackground().getConstantState() == getResources().getDrawable(R.drawable.rounded_button_grey).getConstantState())
//                {
//                    Intent intent = new Intent(MainActivity.this, AddWine.class);
//                    intent.putExtra("position", "x1y2");
//                    startActivity(intent);
//                }else{
//                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
//                    intent.putExtra("position", "x1y2");
//                    startActivity(intent);
//                }
//
//            }
//        });



        for (int i = 0; i < list.size(); i++)
        {
            final Button tempButton = list.get(i);
            tempButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String id_tempBtn =getResources().getResourceEntryName(tempButton.getId());
                    Log.e("****","add id pos => "+id_tempBtn);
                    if(tempButton.getBackground().getConstantState() == getResources().getDrawable(R.drawable.rounded_button_grey).getConstantState())
                    {
                        Intent intent = new Intent(MainActivity.this, AddWine.class);
                        intent.putExtra("position", id_tempBtn);
                        intent.putExtra("id_user", id_user);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                        intent.putExtra("position", id_tempBtn);
                        startActivity(intent);
                    }


                }
            });

        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

        */

        logout();
        return true;
    }

    public void ChooseWineByScan(final String str_id_bottle){
        Cursor bottle = myDb.getWineByID(str_id_bottle);


        if(bottle.getCount()!=0) {
            //afficher nom (1), couleur (3), annee (2)
            bottle.moveToFirst();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle(bottle.getString(1));
            builder.setMessage("Couleur : " + bottle.getString(3) + ", Année : " + bottle.getString(2));
            builder.setPositiveButton("Choose this bottle",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Integer deletedRows = myDb.deleteData(str_id_bottle);
                            if (deletedRows > 0) {
                                Toast.makeText(MainActivity.this, "Data deleted", Toast.LENGTH_LONG).show();
                                //UpdateCave();

                            } else {
                                Toast.makeText(MainActivity.this, "Data not deleted", Toast.LENGTH_LONG).show();
                            }

                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });


            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            Toast.makeText(MainActivity.this, "Bottle not found in the cave", Toast.LENGTH_SHORT).show();
        }
    }

    public void logout(){
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }
    public void UpdateCave(){
        //Cursor res = myDb.getAllData();
        Cursor res = myDb.getWineByUser(id_user);
        Log.e("*****", "vin : "+res.getCount());

        //Log.e("*****", "vin : "+res.getString(3));


        if(res.getCount()!=0){
            while (res.moveToNext()){
                Log.e("****","pos:"+res.getString(5)+" color:"+res.getString(3));
                String position = res.getString(5);
                String color = res.getString(3);
                String var_tmp = "rounded_button_";
                var_tmp +=color;

//                switch(position){
//                    case "x1y2":
//                        if(color.equals("red")){
//                            x1y2.setBackgroundResource(R.drawable.ic_rounded_bottle_red);
//                        }else if(color.equals("white")){
//                            x1y2.setBackgroundResource(R.drawable.ic_rounded_bottle_white);
//                        }else if(color.equals("pink")){
//                            x1y2.setBackgroundResource(R.drawable.ic_rounded_bottle_pink);
//                        }
//                        x1y2.setCompoundDrawables(
//                                null, // Drawable left
//                                null, // Drawable top
//                                null, // Drawable right
//                                null // Drawable bottom
//                        );
//                        break;
//
//                }
                for (int i = 0; i < list.size(); i++)
                {
                    final Button tempButton = list.get(i);

                    String id_tempBtn =getResources().getResourceEntryName(tempButton.getId());

                    if (position.equals(id_tempBtn))
                    {
                        if(color.equals("red")){
                            tempButton.setBackgroundResource(R.drawable.ic_rounded_bottle_red);
                        }else if(color.equals("white")){
                            tempButton.setBackgroundResource(R.drawable.ic_rounded_bottle_white);
                        }else if(color.equals("pink")){
                            tempButton.setBackgroundResource(R.drawable.ic_rounded_bottle_pink);
                        }
                        tempButton.setCompoundDrawables(
                                null, // Drawable left
                                null, // Drawable top
                                null, // Drawable right
                                null // Drawable bottom
                        );
                    }
                }
            }
        }

    }
}




    /*final Button X1Y1 = findViewById(R.id.X1Y1);
    final Button X2Y1 = findViewById(R.id.X2Y1);
    final Button X3Y1 = findViewById(R.id.X3Y1);*/

/* Initialisation couleurs */
    /*arraylist listVins = getAllWine();
    for (int i =0; i<listVins.size(); i++){
        string position = listVins.get(i).getPosition();
        string color = listVins.get(i).getColor();
        string var_tmp = "rounded_button_";
        var_tmp +=color;

        switch(position){
            case "X1Y1"
                X1Y1.setBackground(var_tmp);
                break;
            case "X2Y1"
                break;


        }


    }*/

