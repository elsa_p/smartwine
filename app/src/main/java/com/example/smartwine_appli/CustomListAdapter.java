package com.example.smartwine_appli;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smartwine_appli.user_wine.Wine;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CustomListAdapter  extends BaseAdapter {

    private List<Wine> listData;
    ArrayList<Wine> arraylist;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListAdapter(Context aContext,  List<Wine> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        this.arraylist = new ArrayList<Wine>();
        this.arraylist.addAll(listData);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_layout, null);
            holder = new ViewHolder();
            holder.wineView = (ImageView) convertView.findViewById(R.id.imageView_wine);
            holder.wineNameView = (TextView) convertView.findViewById(R.id.textView_wineName);
            holder.yearView = (TextView) convertView.findViewById(R.id.textView_year);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Wine wine = this.listData.get(position);
        holder.wineNameView.setText(wine.getName());
        holder.yearView.setText("Year: " + wine.getYear());

        //holder.wineView.setImageDrawable(new ColorDrawable(tweet.getColor()));
        //holder.wineView.setImageDrawable(new ColorDrawable(23));


        //int imageId = this.getMipmapResIdByName(wine.getColor());
        int imageId = this.getMipmapResIdByName(wine.getColor());

        holder.wineView.setImageResource(imageId);

        return convertView;
    }

    // Find Image ID corresponding to the name of the image (in the directory mipmap).
    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();
        // Return 0 if not found.
        int resID = context.getResources().getIdentifier(resName , "mipmap", pkgName);
        Log.i("CustomListView", "Res Name: "+ resName+"==> Res ID = "+ resID);
        return resID;
    }

    public void filter(String charText){
        charText = charText.toLowerCase(Locale.getDefault());
        listData.clear();
        if (charText.length() == 0) {
            listData.addAll(arraylist);
        }
        else
        {
            for (Wine wine : arraylist)
            {
                if (wine.getName().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listData.add(wine);
                }
                if (wine.getYear().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listData.add(wine);
                }
            }
        }
        notifyDataSetChanged();
    }


    public void filterWine(ArrayList<String> filtertab){
        //parcourir tab des vins
        boolean isFilterMeal=false;
        boolean isFilterColor=false;
        boolean isFilterYear = false;
        //Log.e("**** : "+ listData.size(), " vin : " + listData.get(1).toString());
        listData.clear();
        for(int i=0 ; i<arraylist.size() ; i++){
            //
            /*if(filtertab.get(1) != ""){
                if(arraylist.get(i).getYear().equals(filtertab.get(1))){
                    listData.add(arraylist.get(i));
                }
            }*/
            //parcourir filtertab
            //si couleur sélectionnée on ajoute
           // if(filtertab.get(1) !=""){
                //isFilterYear = true;
                //if(arraylist.get(i).toString().contains(filtertab.get(1))){ //check couleur
                    for(int j=1 ; j<4; j++){ //parcourt couleur filtres
                        if(filtertab.get(j) != ""){ //filtre couleur non vide
                            isFilterColor = true;
                            if(arraylist.get(i).toString().contains(filtertab.get(j))){ //check couleur filtre dans vin
                                for(int r=4 ; r<filtertab.size();r++){ //parcourt repas filtres
                                    if(filtertab.get(r) != "") { //filtre repas non vide
                                        isFilterMeal= true;
                                        if (arraylist.get(i).toString().contains(filtertab.get(r))) {
                                            listData.add(arraylist.get(i));
                                            break;//pas besoin de tester tous les repas, si 1 repas contenu on ajoute
                                        }
                                    }
                                }
                                if(!isFilterMeal){ //si pas de filtre repas on ajoute vin de couleur filtre
                                    listData.add(arraylist.get(i));
                                }

                            }
                        }

                    }
                    /*if(!isFilterColor){
                        listData.add(arraylist.get(i));
                    }*/
               // }

            //}
            if(!isFilterColor){ //si pas de filtre couleur, on regarde filtre repas
                for(int r=4 ; r<filtertab.size();r++){ //parcourt filtre repas
                    if(filtertab.get(r) != "") { //filtre repas non vide
                        isFilterMeal= true;
                        if (arraylist.get(i).toString().contains(filtertab.get(r))) {
                            listData.add(arraylist.get(i));
                            break;//pas besoin de tester tous les repas, si 1 on ajoute
                        }
                    }
                }
            }
            /*if(!isFilterYear){
                for(int j=1 ; j<4; j++){
                    if(filtertab.get(j) != ""){
                        isFilterColor = true;
                        if(arraylist.get(i).toString().contains(filtertab.get(j))){
                            for(int r=4 ; r<filtertab.size();r++){
                                if(filtertab.get(r) != "") {
                                    isFilterMeal= true;
                                    if (arraylist.get(i).toString().contains(filtertab.get(r))) {
                                        listData.add(arraylist.get(i));
                                        break;//pas besoin de tester tous les repas, si 1 on ajoute
                                    }
                                }
                            }
                            if(!isFilterMeal){
                                listData.add(arraylist.get(i));
                            }

                        }
                    }

                }
            }*/


            //MARCHE PAS TOTALEMENT MAIS PAS MAL :
            /*for(int j=0 ; j<filtertab.size();j++){
                //si filtertab(j) non vide -> un filtre est présent
                if(filtertab.get(j) != "") {
                    //si filter(j) non contenu dans le vin -> vin retiré de la liste
                    if(!arraylist.get(i).toString().contains(filtertab.get(j))) {
                        listData.remove(arraylist.get(i));
                    }

                }
            }*/



            //parcourir filtertab
            //si filtertab(j) n'est pas vide
            //si filtertab(j) est contenu dans le vin

            /*if(arraylist.get(i).getYear() == filtertab.get(1) && arraylist.get(i).getYear() == intent_year){
                listData.add(arraylist.get(i));
            }*/
        }
        if(listData.isEmpty()){
            listData.addAll(arraylist);

            if(isFilterColor || isFilterMeal){
                listData.clear();
            }
            /*if(filtertab.get(0) !=""){
                for(int w=0; w<listData.size();w++){
                    if(!listData.get(w).getYear().equals(filtertab.get(0))){
                        listData.remove(listData.get(w));
                        //w=w-1;
                    }
                }
            }*/
        }
        /*else{
            if(filtertab.get(0) != ""){
                for(int w=0; w<listData.size();w++){
                    if(!listData.get(w).getYear().equals(filtertab.get(0))){
                        listData.remove(listData.get(w));
                    }
                }
            }
        }*/
        notifyDataSetChanged();
    }
    public void filter_options(String charText){
        charText = charText.toLowerCase(Locale.getDefault());
        Log.e("**** : "+ charText, " text : " + charText);

        listData.clear();
        if (charText.length() == 0) {
            listData.addAll(arraylist);
        }
        else
        {
            for (Wine wine : arraylist)
            {
                if (wine.getColor().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listData.add(wine);
                }
                if (wine.getYear().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listData.add(wine);
                }
                if (wine.getMeal().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    listData.add(wine);
                }
            }
        }
        notifyDataSetChanged();
    }
/*    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                listData = (ArrayList<Wine>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null && constraint.length() > 0) {
                    ArrayList<Wine> filterList = new ArrayList<Wine>();
                    for (int i = 0; i < listData.size(); i++) {
                        if ((listData.get(i).getName().toUpperCase())
                                .contains(constraint.toString().toUpperCase())) {
                            Wine winedata = new Wine(listData.get(i).getId(),listData.get(i)
                                    .getName(), listData.get(i).getYear(), listData.get(i).getColor());
                            filterList.add(winedata);
                        }
                    }
                    results.count = filterList.size();
                    results.values = filterList;
                } else {
                    results.count = listData.size();
                    results.values = listData;
                }
                return results;
            }
        };

        return filter;
    }*/

    public class ViewHolder {
        ImageView wineView;
        TextView wineNameView;
        TextView yearView;
    }


}
