package com.example.smartwine_appli;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.smartwine_appli.data.DatabaseHandler;
import com.example.smartwine_appli.user_wine.Wine;

import java.util.ArrayList;

public class DataHelper {


    /*public static ArrayList<Wine> loadWine(Context context) {
        ArrayList<Wine> wines = new ArrayList<>();

        String json = "";

        try {
            InputStream is = context.getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try {
            JSONObject obj = new JSONObject(json);
            JSONArray jsonArray = obj.getJSONArray("listeVins");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Wine wine = new Wine();
                wine.setId(jsonObject.getString("id"));
                wine.setName(jsonObject.getString("name"));
                wine.setColor(jsonObject.getString("color"));
                wine.setYear(jsonObject.getString("year"));
                wine.setMeal(jsonObject.getString("meal"));
                wine.setPosition(jsonObject.getString("position"));

                wines.add(wine);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return wines;
    }*/


    public static ArrayList<Wine> loadWineV2(Context context, String id_user){
        ArrayList<Wine> wines = new ArrayList<>();
        DatabaseHandler myDb = new DatabaseHandler(context);
        //Cursor res = myDb.getAllData();
        Cursor res = myDb.getWineByUser(id_user);
        if(res.getCount()==0){
            //show message
            Log.e("*****", "NO DATA LOADWINEV2");
        }

        while(res.moveToNext()){

            Wine wine = new Wine();
            wine.setId(res.getString(0));
            wine.setName(res.getString(1));
            wine.setColor(res.getString(3));
            wine.setYear(res.getString(2));
            wine.setMeal(res.getString(4));
            wine.setPosition(res.getString(5));

            wines.add(wine);
        }
        return wines;
    }
}
