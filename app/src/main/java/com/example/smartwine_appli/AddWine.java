package com.example.smartwine_appli;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartwine_appli.data.DatabaseHandler;


public class AddWine extends AppCompatActivity {

    private Toolbar mToolbar;
    public DatabaseHandler myDb;
    private String color;
    private String meal="";
    private EditText editName ;
    private EditText editYear ;
    private TextView id ;
    //private Button button_addwine ;
    static String position;
    static String id_user;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDb = new DatabaseHandler(this);

        setContentView(R.layout.activity_add_wine);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        editName = (EditText) findViewById(R.id.edit_name);
        editYear = (EditText) findViewById(R.id.edit_year);
        id = (TextView) findViewById(R.id.auto_id);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Button button_scan = findViewById(R.id.scan);
        button_scan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Intent intent = new Intent(AddWine.this, ReadingBarCode.class);
                intent.putExtra("position", position);
                intent.putExtra("id_user", id_user);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        if (intent != null){
            Log.e("*******", "i ="+i);
            // Le code pour récupérer les extras ira ici
            //if(i==2) {
            String str_id = "";
            if (intent.hasExtra("edittext")) { // vérifie qu'une valeur est associée à la clé “edittext”
                str_id = intent.getStringExtra("edittext"); // on récupère la valeur associée à la clé

            }
            TextView textView = (TextView) findViewById(R.id.auto_id);
            textView.setText(str_id);
            //}
            //else if(i!=2){
            if(intent.hasExtra("position")){
                //i=2;
                position = intent.getStringExtra("position");
                Log.e("****", "position_intent=> "+position+"i="+i);
                String tmp = position;
                setPosition(tmp);
                //setPosition(position);
                /*final Button button_addwine = findViewById(R.id.add_to_cave);//button_addwine.setOnClickListener(new View.OnClickListener() {

                button_addwine.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // Code here executes on main thread after user presses button
                        String position = "x1y1";
                        String meal = "aperitif meat cheese";
                        Log.e("****", "meal = " + meal +" || position = "+position);
                        boolean isInserted = myDb.insertData(id.getText().toString(), editName.getText().toString(), editYear.getText().toString(),color,meal, position);
                        Log.e("****", "position=> "+position);

                        if(isInserted == true){
                            Toast.makeText(AddWine.this, "Data inserted", Toast.LENGTH_LONG).show();

                        }else{
                            Toast.makeText(AddWine.this, "Data not inserted", Toast.LENGTH_LONG).show();
                        }

                        Intent intent = new Intent(AddWine.this, MainActivity.class);
                        startActivity(intent);
                    }
                });*/
            }
            if(intent.hasExtra("id_user")){
                id_user = intent.getStringExtra("id_user");

            }
            //}

        }



        //addData();

        View.OnClickListener test = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                //String position = getPosition();
                //String meal = "aperitif fish cheese dessert";
                addWine();

            }
        };

        final Button button_addwine = findViewById(R.id.add_to_cave);
        button_addwine.setOnClickListener(test);


        //button_addwine.setOnClickListener(new View.OnClickListener() {

            /*public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Log.e("****", "meal = " + meal +" || position = "+position);
                boolean isInserted = myDb.insertData(id.getText().toString(), editName.getText().toString(), editYear.getText().toString(),color,meal, position);
                Log.e("****", "position=> "+position);

                if(isInserted == true){
                    Toast.makeText(AddWine.this, "Data inserted", Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(AddWine.this, "Data not inserted", Toast.LENGTH_LONG).show();
                }

                Intent intent = new Intent(AddWine.this, MainActivity.class);
                startActivity(intent);
            }
        });*/
    }



    public void setPosition(String pos){
        this.position=pos;
    }
    public String getPosition(){
        return this.position;
    }

    public void setId_user(String idu){
        this.id_user=idu;
    }
    public String getId_user(){
        return this.id_user;
    }

    public void addWine(){
        position = getPosition();
        Log.e("****", "meal = " + meal +" || position = "+position);
        boolean isInserted = myDb.insertData(id.getText().toString(), editName.getText().toString(), editYear.getText().toString(),color,meal, position, id_user);
        Log.e("****", "position=> "+position);
        Log.e("****", "id_user=> "+id_user);

        if(isInserted == true){
            Toast.makeText(AddWine.this, "Data inserted", Toast.LENGTH_LONG).show();

        }else{
            Toast.makeText(AddWine.this, "Data not inserted", Toast.LENGTH_LONG).show();
        }

        Intent intent = new Intent(AddWine.this, MainActivity.class);
        startActivity(intent);
    }

    /*public void addData(){
        button_addwine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                position = getPosition();
                Log.e("****", "position=> "+position);
                boolean isInserted = myDb.insertData(id.getText().toString(), editName.getText().toString(), editYear.getText().toString(),color,meal, position);
                Log.e("****", "position=> "+position);

                if(isInserted == true){
                    Toast.makeText(AddWine.this, "Data inserted", Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(AddWine.this, "Data not inserted", Toast.LENGTH_LONG).show();
                }

                Intent intent = new Intent(AddWine.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }*/
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_addwine, menu);
        return true;
    }

    public void viewAll(){
        Cursor res = myDb.getAllData();
        if(res.getCount()==0){
            //show message
            showMessage("Error", "Nothing found");
            return;
        }
        StringBuffer buffer = new StringBuffer();
        while(res.moveToNext()){
            buffer.append("Id :"+res.getString(0)+"\n");
            buffer.append("Name :"+res.getString(1)+"\n");
            buffer.append("Year :"+res.getString(2)+"\n");
            buffer.append("Color :"+res.getString(3)+"\n");
            buffer.append("Meal :"+res.getString(4)+"\n");
            buffer.append("Position :" + res.getString(5)+"\n\n");

        }
        //show all data
        showMessage("Data",buffer.toString());
    }


    public void UpdateData(){
        /*btnviewUpdate.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        boolean isUpdate = myDb.updateData(id.getText().toString(),editName.getText().toString(), editYear.getText().toString(), color, meal);
                        if(isUpdate == true){
                            Toast.makeText(AddWine.this, "Data updated", Toast.LENGTH_LONG).show();

                        }else{
                            Toast.makeText(AddWine.this, "Data not updated", Toast.LENGTH_LONG).show();

                        }

                    }
                }
        )*/
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        ImageView colorbottle = findViewById(R.id.imageView_wine);
        boolean checked = ((CheckBox) view).isChecked();
        String tmp_meal = "";
        // Check which checkbox was clicked
        int idView = view.getId();
        switch(idView) {
            case R.id.cb_rouge:
                if (checked) {
                    color = "red";
                    colorbottle.setImageResource(R.mipmap.red);
                }
                else
                    colorbottle.setImageResource(R.mipmap.dark);
                break;
            case R.id.cb_blanc:
                if (checked) {
                    color = "white";
                    colorbottle.setImageResource(R.mipmap.white);
                }
                else
                    colorbottle.setImageResource(R.mipmap.dark);
                break;
            case R.id.cb_rose:
                if (checked) {
                    color = "pink";
                    colorbottle.setImageResource(R.mipmap.pink);
                }
                else
                    colorbottle.setImageResource(R.mipmap.dark);
                break;
        }

    }

    public void onCheckboxClicked_meal(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        int idView = view.getId();
        switch(idView) {
            case R.id.cb_apero:
                if (checked) {
                    Log.e("****","cb_apero");
                    meal = meal + "aperitif ";
                }else{
                    meal = meal.replace("aperitif ","");
                }
                break;
            case R.id.cb_viande:
                if (checked) {
                    Log.e("****","cb_viande");
                    meal = meal + "meat ";
                }
                else{
                    meal = meal.replace("meat ","");
                }
                break;
            case R.id.cb_poisson:
                if (checked) {
                    Log.e("****","cb_poisson");
                    meal = meal + "fish ";
                }
                else{
                    meal = meal.replace("fish ","");
                }
                break;
            case R.id.cb_fromage:
                if (checked) {
                    Log.e("****","cb_fromage");
                    meal = meal + "cheese ";
                }
                else{
                    meal = meal.replace("cheese ","");
                }
                break;
            case R.id.cb_dessert:
                if (checked) {
                    Log.e("****","cb_dessert");
                    meal = meal + "dessert ";
                }
                else{
                    meal = meal.replace("dessert ","");
                }
                break;
        }
        Log.e("****","tmp_meal => " + meal);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

        */
        Intent intent = new Intent(AddWine.this, MainActivity.class);
        startActivity(intent);
        return true;
    }


}
