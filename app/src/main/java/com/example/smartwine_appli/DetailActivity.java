package com.example.smartwine_appli;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartwine_appli.data.DatabaseHandler;

public class DetailActivity extends AppCompatActivity {

    private TextView name;
    private TextView id;
    private TextView year;
    private TextView color;
    private TextView meal;
    private ImageView imagewine;

    DatabaseHandler myDb;
    private Button button_choosewine;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myDb = new DatabaseHandler(this);

        button_choosewine = findViewById(R.id.choose_wine);

        name = findViewById(R.id.name_wine);
        id = findViewById(R.id.id_wine);
        year = findViewById(R.id.year_wine);
        color= findViewById(R.id.color_wine);
        meal = findViewById(R.id.meal_wine);
        imagewine = findViewById(R.id.imageView_wine);

        Bundle extra = getIntent().getExtras();
        if (extra != null){
            String id_extra = extra.getString("EXTRA_ID");
            String name_extra = extra.getString("EXTRA_NAME");
            String year_extra = extra.getString("EXTRA_YEAR");
            String color_extra = extra.getString("EXTRA_COLOR");
            String meal_extra = extra.getString("EXTRA_MEAL");
            String position_extra = extra.getString("EXTRA_POSITION");

            id.setText(id_extra);
            name.setText(name_extra);
            year.setText(year_extra);
            color.setText(color_extra);
            meal.setText(meal_extra);

            if("red".equals(color_extra)){
                imagewine.setImageResource(R.mipmap.red);
            }
            if("white".equals(color_extra)){
                imagewine.setImageResource(R.mipmap.white);
            }
            if("pink".equals(color_extra)){
                imagewine.setImageResource(R.mipmap.pink);
            }


        }
        Intent intent = getIntent();
        if (intent != null){
            if (intent.hasExtra("position")) { // vérifie qu'une valeur est associée à la clé “edittext”
                String position = intent.getStringExtra("position"); // on récupère la valeur associée à la clé
                Cursor wine = myDb.getWineByPosition(position);
                while (wine.moveToNext()) {
                    id.setText(wine.getString(0));
                    name.setText(wine.getString(1));
                    year.setText(wine.getString(2));
                    color.setText(wine.getString(3));
                    meal.setText(wine.getString(4));

                    if ("red".equals(wine.getString(3))) {
                        imagewine.setImageResource(R.mipmap.red);
                    }
                    if ("white".equals(wine.getString(3))) {
                        imagewine.setImageResource(R.mipmap.white);
                    }
                    if ("pink".equals(wine.getString(3))) {
                        imagewine.setImageResource(R.mipmap.pink);
                    }
                }

            }
        }

        ChooseWineByDetails();


    }

    public void ChooseWineByDetails(){
        button_choosewine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                Log.e("*****"," Delete => " + id.getText().toString());

                Integer deletedRows = myDb.deleteData(id.getText().toString());
                if(deletedRows > 0){
                    Toast.makeText(DetailActivity.this, "Data deleted", Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(DetailActivity.this, "Data not deleted", Toast.LENGTH_LONG).show();
                }

                Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getWineByPosition(String position){
        Cursor res = myDb.getWineByPosition(position);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailwine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

        */
        Intent intent = new Intent(DetailActivity.this, MainActivity.class);
        startActivity(intent);
        return true;
    }

}
