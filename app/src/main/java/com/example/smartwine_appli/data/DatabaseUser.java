package com.example.smartwine_appli.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseUser extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "User.db";
    public static final String TABLE_NAME = "user_table";
    public static final String COL1 = "ID";
    public static final String COL2  = "PASSWORD";
    //public static final ArrayList<Wine> COL3 = new ArrayList<Wine>();

    private static final int DATABASE_VERSION = 1;
    public static final String USER_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + "(ID TEXT PRIMARY KEY, PASSWORD TEXT  )";

    public DatabaseUser(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USER_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertUser(String id, String password){
        Log.e("****", "fct insertUser");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, id);
        contentValues.put(COL2, password);
        Log.e("****", "Before insert");
        long result = db.insert(TABLE_NAME, null, contentValues);
        Log.e("****", "After insert => "+result);
        if(result == -1){
            Log.e("****  ", "Insert fail");
            return false;
        }else{
            Log.e("****"  , "Insert succes");
            return true;
        }

    }

    public Integer deleteUser (String id){
        Log.e("****","Delete fct with id="+id);
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new String[] {id});
    }

    public Cursor getAllUser(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }

    public Cursor getUserByID(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME +" where id = ?", new String[] {id});
        return res;
    }
    public Cursor getUserByIDandPassword(String id, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME +" where id = ? and password = ?", new String[] {id, password});
        return res;
    }
}
