package com.example.smartwine_appli.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
//créer et mettre à jour base de données

    private static final String DATABASE_NAME = "Wine.db";
    public static final String TABLE_NAME = "wine_table";
    public static final String COL1 = "ID";
    public static final String COL2  = "NAME";
    public static final String COL3 = "YEAR";
    public static final String COL4  = "COLOR";
    public static final String COL5  = "MEAL";
    public static final String COL6 = "POSITION";
    public static final String COL7 = "ID_USER";

    private static final int DATABASE_VERSION = 4;

    /*public static final String WINE_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    ID_KEY + "TEXT" +
                    COLOR + "TEXT, " +
                    YEAR + " TEXT, " +
                    MEAL + "TEXT);";*/

    public static final String WINE_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + "(ID TEXT, NAME TEXT, YEAR TEXT, COLOR TEXT,MEAL TEXT, POSITION TEXT, ID_USER TEXT, PRIMARY KEY(ID, ID_USER))";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(WINE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String id, String name, String year, String color, String meal, String position, String id_user){
        Log.e("****", "fct insertData");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, id);
        contentValues.put(COL2, name);
        contentValues.put(COL3, year);
        contentValues.put(COL4, color);
        contentValues.put(COL5, meal);
        contentValues.put(COL6, position);
        contentValues.put(COL7, id_user);

        Log.e("****", "Before insert");
        long result = db.insert(TABLE_NAME, null, contentValues);
        Log.e("****", "After insert => "+result);
        if(result == -1){
            Log.e("****  ", "Insert fail");
            return false;
        }else{
            Log.e("****"  , "Insert succes");
            return true;
        }

    }

    public Integer deleteData(String id){
        Log.e("****","Delete fct with id="+id);
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new String[] {id});
    }


    public void deleteAllData(){
        Log.e("****","Delete all data");
        Cursor res= this.getAllData();
        while(res.moveToNext()){
            this.deleteData(res.getString(0));
        }
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }

    public Cursor getWineByPosition(String position){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME +" where position = ?", new String[] {position});
        return res;
    }

    public Cursor getWineByUser(String id_user){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME +" where id_user = ?", new String[] {id_user});
        return res;
    }

    public Cursor getWineByID(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME +" where id = ?", new String[] {id});
        return res;
    }

    public boolean updateData(String id, String name, String year, String color, String meal, String position, String id_user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, name);
        contentValues.put(COL3, year);
        contentValues.put(COL4, color);
        contentValues.put(COL5, meal);
        contentValues.put(COL6, position);
        contentValues.put(COL7, id_user);

        db.update(TABLE_NAME, contentValues, "ID = ?", new String[] {id});
        return true;

    }
}
