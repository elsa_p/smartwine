package com.example.smartwine_appli.user_wine;

public class Wine {

    private String id;
    private String name;
    private String year;
    private String color;
    private String meal;
    private String position;
    private String id_user;

    public Wine(String id, String name, String year, String color, String meal, String position, String id_user){
        this.id = id;
        this.name = name;
        this.color = color;
        this.year = year;
        this.meal = meal;
        this.position = position;
        this.id_user=id_user;
    }

    public Wine() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    public String getIdUser() {
        return id_user;
    }

    public void setIdUser(String id_user) {
        this.id_user = id_user;
    }




    @Override
    public String toString() {
        return "Wine{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", color='" + color + '\'' +
                ", meal='" + meal + '\'' +
                ", position='" + position + '\'' +
                ", id user='" + id_user + '\'' +
                '}';
    }
}
