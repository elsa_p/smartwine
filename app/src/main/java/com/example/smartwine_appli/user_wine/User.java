package com.example.smartwine_appli.user_wine;

import java.util.ArrayList;

public class User {

    private String id;
    private String password;
    private ArrayList<Wine> wines;

    public User(String id, String password, ArrayList<Wine> wines) {
        this.id = id;
        this.password = password;
        this.wines = wines;
}

    public User(String id, String password) {
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Wine> getWines() {
        return wines;
    }

    public void setWines(ArrayList<Wine> wines) {
        this.wines = wines;
    }



}
