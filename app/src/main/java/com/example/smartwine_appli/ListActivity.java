package com.example.smartwine_appli;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;

import com.example.smartwine_appli.user_wine.Wine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class ListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView lv;
    private ArrayList<Wine> wineList;
    private ArrayList<String> filtertab = new ArrayList<String>(Arrays.asList("","","","","","","","",""));
    private ArrayList<String> nameList;
    CustomListAdapter adapter;

    SearchView searchview;
    EditText inputSearch;


    private Toolbar mToolbar;
    static public String id_user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("id_user")) {
                id_user = intent.getStringExtra("id_user");
            }
        }
        lv = (ListView) findViewById(R.id.listView);
        wineList = DataHelper.loadWineV2(this, id_user);
        nameList = new ArrayList<>();

        for (int i =0; i<wineList.size();i++){
            String str = wineList.get(i).getName() + "\n" + wineList.get(i).getYear();
            nameList.add(str);
            // Binds all strings into an array
        }

        final CustomListAdapter adapter_wine = new CustomListAdapter(ListActivity.this, wineList);
        lv.setAdapter(adapter_wine);


        //get data from filter activity
        //Intent intent = getIntent();
        if (intent != null){
            // Le code pour récupérer les extras ira ici
            Log.e("**** : avant if", "aaaa");
            if (intent.hasExtra("string_year")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_year = intent.getStringExtra("string_year"); // on récupère la valeur associée à la clé

                if(str_year!=""){
                    //adapter_wine.filter_options(str_year);
                    filtertab.set(0, str_year);
                }else{
                    filtertab.set(0, "");
                }
            }
            if (intent.hasExtra("string_red")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_red = intent.getStringExtra("string_red"); // on récupère la valeur associée à la clé

                Log.e("**** : "+ str_red, " color : " + str_red);
                if(str_red!=""){
                    //adapter_wine.filter_options(str_red);
                    filtertab.set(1, str_red);
                }else{
                    filtertab.set(1, "");
                }

            }
            if (intent.hasExtra("string_white")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_white = intent.getStringExtra("string_white"); // on récupère la valeur associée à la clé
                if(str_white!=""){
                    //adapter_wine.filter_options(str_white);
                    filtertab.set(2, str_white);
                }else{
                    filtertab.set(2, "");
                }

            }
            if (intent.hasExtra("string_pink")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_pink = intent.getStringExtra("string_pink"); // on récupère la valeur associée à la clé
                if(str_pink!=""){
                    filtertab.set(3, str_pink);
                }else{
                    filtertab.set(3, "");
                }
            }
            if (intent.hasExtra("string_aperitif")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_aperitif = intent.getStringExtra("string_aperitif"); // on récupère la valeur associée à la clé
                if(str_aperitif!=""){
                    filtertab.set(4, str_aperitif);
                }else{
                    filtertab.set(4, "");
                }
            }
            if (intent.hasExtra("string_meat")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_meat = intent.getStringExtra("string_meat"); // on récupère la valeur associée à la clé
                if(str_meat!=""){
                    filtertab.set(5, str_meat);
                }else{
                    filtertab.set(5, "");
                }
            }
            if (intent.hasExtra("string_fish")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_fish = intent.getStringExtra("string_fish"); // on récupère la valeur associée à la clé
                if(str_fish!=""){
                    filtertab.set(6, str_fish);
                }else{
                    filtertab.set(6, "");
                }
            }
            if (intent.hasExtra("string_cheese")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_cheese = intent.getStringExtra("string_cheese"); // on récupère la valeur associée à la clé
                if(str_cheese!=""){
                    filtertab.set(7, str_cheese);
                }else{
                    filtertab.set(7, "");
                }
            }
            if (intent.hasExtra("string_dessert")){ // vérifie qu'une valeur est associée à la clé “edittext”
                String str_dessert = intent.getStringExtra("string_dessert"); // on récupère la valeur associée à la clé
                if(str_dessert!=""){
                    filtertab.set(7, str_dessert);
                }else{
                    filtertab.set(7, "");
                }
            }
            adapter_wine.filterWine(filtertab);


        }

        //adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nameList);
        //lv.setAdapter((ListAdapter) adapter);


        //filter with bar search
        inputSearch = (EditText) findViewById(R.id.search_view);

        //lv.setAdapter(new CustomListAdapter(this,wineList));
        lv.setOnItemClickListener(this);
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                //adapter.getFilter().filter(cs);
                String text = inputSearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter_wine.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        /*final ImageButton button_filter = findViewById(R.id.but_filter);
        button_filter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                Intent intent = new Intent(ListActivity.this, Filter.class);
                startActivity(intent);
            }
        });*/


        final ImageButton but_sort_acr = findViewById(R.id.but_croissant);

        but_sort_acr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Collections.sort(wineList, new Comparator<Wine>() {
                    @Override
                    public int compare(Wine tc1, Wine tc2) {
                        return Integer.parseInt(tc1.getYear()) - Integer.parseInt(tc2.getYear());
                    }
                });

                lv.setAdapter(adapter_wine);
                Log.e("*****",wineList.toString());

            }
        });
        final ImageButton but_sort_decr = findViewById(R.id.but_decroissant);

        but_sort_decr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Collections.sort(wineList, new Comparator<Wine>() {
                    @Override
                    public int compare(Wine tc1, Wine tc2) {
                        return 1-(Integer.parseInt(tc1.getYear()) - Integer.parseInt(tc2.getYear()));
                    }
                });

                lv.setAdapter(adapter_wine);
                Log.e("*****",wineList.toString());

            }
        });







        final ImageButton button_refresh = findViewById(R.id.but_refresh);
        button_refresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Code here executes on main thread after user presses button
                String str = "";
                adapter_wine.filter(str);
                inputSearch = (EditText) findViewById(R.id.search_view);
                inputSearch.setText("");
            }
        });

    }

   

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(ListActivity.this, DetailActivity.class);
        String Id = wineList.get(position).getId();
        String name = wineList.get(position).getName();
        String color = wineList.get(position).getColor();
        String year = wineList.get(position).getYear();
        String meal = wineList.get(position).getMeal();
        String pos = wineList.get(position).getPosition();

        intent.putExtra("EXTRA_ID", Id);
        intent.putExtra("EXTRA_NAME", name);
        intent.putExtra("EXTRA_COLOR", color);
        intent.putExtra("EXTRA_YEAR", year);
        intent.putExtra("EXTRA_MEAL", meal);
        intent.putExtra("EXTRA_POSITION", pos);

        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

        */
        int id = item.getItemId();

        if(id == R.id.action_cave) {
            Intent intent = new Intent(ListActivity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.action_filter){
            Intent intent = new Intent(ListActivity.this, Filter.class);
            startActivity(intent);
            return true;
        }else{
            return false;
        }

    }



}